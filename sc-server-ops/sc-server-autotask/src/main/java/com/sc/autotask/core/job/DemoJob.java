package com.sc.autotask.core.job;



import com.alibaba.fastjson.JSONObject;
import com.sc.common.annotations.Job;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.demo1.entity.demo.Demo;
import com.sc.demo1.api.DemoService;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Created by wust on 2019/6/13.
 * 五秒钟执行一次
 */

@Component
@Job(jobName="demo1-job",jobDescription="demo",jobGroupName="sc-demo1",cronExpression="*/5 * * * * ?")
public class DemoJob extends BaseJob {

    @Autowired
    private DemoService demoService;

    @Override
    public void run(JobExecutionContext jobExecutionContext) {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        Demo demo = new Demo();
        WebResponseDto webResponseDto = demoService.select(JSONObject.toJSONString(ctx),demo);
        System.out.println("结果="+webResponseDto);
    }
}
