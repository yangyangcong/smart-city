package com.sc.businesslog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author ：wust
 * @date ：Created in 2019/8/26 11:01
 * @description：
 * @version:
 */
@EnableSwagger2
@EnableEurekaClient
@EnableTransactionManagement
@MapperScan(basePackages = {"com.sc.common.dao","com.sc.businesslog.core.dao"})
@ComponentScan(basePackages  = {"com.sc"},
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                classes = {}))
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class BusinessLogServerApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(BusinessLogServerApplication.class);
        app.setWebApplicationType(WebApplicationType.REACTIVE);
        SpringApplication.run(BusinessLogServerApplication.class, args);
    }
}
