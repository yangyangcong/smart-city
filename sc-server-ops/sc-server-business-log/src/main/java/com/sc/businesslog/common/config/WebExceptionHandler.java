package com.sc.businesslog.common.config;

import com.sc.common.dto.WebResponseDto;
import com.sc.common.exception.BaseWebExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class WebExceptionHandler extends BaseWebExceptionHandler {
    @Override
    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public WebResponseDto handle(Throwable e) {
        return super.handle(e);
    }
}
