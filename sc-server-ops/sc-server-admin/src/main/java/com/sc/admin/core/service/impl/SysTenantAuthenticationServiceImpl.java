package com.sc.admin.core.service.impl;

import com.sc.admin.core.dao.SysTenantAuthenticationMapper;
import com.sc.admin.core.service.SysTenantAuthenticationService;
import com.sc.admin.entity.tenant.SysTenantAuthentication;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import com.sc.common.dto.WebResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: wust
 * @date: 2020-12-03 10:21:44
 * @description:
 */
@Service("sysTenantAuthenticationServiceImpl")
public class SysTenantAuthenticationServiceImpl extends BaseServiceImpl<SysTenantAuthentication> implements SysTenantAuthenticationService{
    @Autowired
    private SysTenantAuthenticationMapper sysTenantAuthenticationMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysTenantAuthenticationMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
