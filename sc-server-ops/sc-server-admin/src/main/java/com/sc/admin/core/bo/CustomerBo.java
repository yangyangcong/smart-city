/**
 * Created by wust on 2020-04-09 10:04:23
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.bo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysCustomerMapper;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.entity.admin.customer.SysCustomer;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;
import java.util.List;

/**
 * @author: wust
 * @date: Created in 2020-04-09 10:04:23
 * @description:
 *
 */
@Component
public class CustomerBo {
    @Autowired
    private SysCustomerMapper sysCustomerMapper;

    public JSONArray buildCascader(){
        final JSONArray jsonArray = new JSONArray();

        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        Example example = new Example(SysCustomer.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("projectId",ctx.getProjectId());
        criteria.andEqualTo("branchCompanyId",ctx.getBranchCompanyId());
        List<SysCustomer> customers =  sysCustomerMapper.selectByExample(example);
        if(CollectionUtils.isNotEmpty(customers)){
            for (SysCustomer customer : customers) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("value",customer.getId());
                jsonObject.put("label",customer.getLoginName() + "(" +customer.getName() + ")");
                jsonArray.add(jsonObject);
            }
        }
        return jsonArray;
    }
}
