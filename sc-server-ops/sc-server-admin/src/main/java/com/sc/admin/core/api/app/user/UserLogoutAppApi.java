/**
 * Created by wust on 2019-11-05 10:34:41
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.app.user;


import com.sc.common.annotations.OperationLog;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.cache.SpringRedisTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;

/**
 * @author: wust
 * @date: Created in 2019-11-05 10:34:41
 * @description: 退出登录
 *
 */
@Api(tags = {"App接口~员工登出"})
@RequestMapping("/api/app/v1/UserLogoutAppApi")
@RestController
public class UserLogoutAppApi {
    @Autowired
    private SpringRedisTools springRedisTools;

    @ApiOperation(value = "App员工登出", httpMethod = "POST")
    @OperationLog(moduleName= OperationLogEnum.MODULE_COMMON,businessName="App用户登出",operationType= OperationLogEnum.Logout)
    @RequestMapping(value = "/logout/{xAuthToken}",method = RequestMethod.POST)
    public WebResponseDto logout(@PathVariable String xAuthToken) {
        WebResponseDto responseDto = new WebResponseDto();

        String redisKey = null;
        try {
            redisKey = new String(Base64.decodeBase64(xAuthToken), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            responseDto.setFlag(WebResponseDto.INFO_ERROR);
            responseDto.setMessage("注销失败");
            return responseDto;
        }

        if(springRedisTools.hasKey(redisKey)){
            springRedisTools.deleteByKey(redisKey);
        }

        return responseDto;
    }
}
