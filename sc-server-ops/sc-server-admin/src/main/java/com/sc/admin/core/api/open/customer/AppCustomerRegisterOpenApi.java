/**
 * Created by wust on 2019-10-30 18:02:26
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.open.customer;


import com.sc.admin.core.service.SysCustomerService;
import com.sc.common.annotations.OpenApi;
import com.sc.common.annotations.OperationLog;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.entity.admin.customer.SysCustomer;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.util.MyStringUtils;
import com.sc.common.util.RC4;
import com.sc.common.util.cache.SpringRedisTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;

/**
 * @author: wust
 * @date: Created in 2019-10-30 18:02:26
 * @description: 客户注册
 *
 */
@Api(tags = {"开放接口~app客户注册"})
@OpenApi
@RequestMapping("/api/open/v1/AppCustomerRegisterOpenApi")
@RestController
public class AppCustomerRegisterOpenApi {

    @Autowired
    private SysCustomerService sysCustomerServiceImpl;

    @Autowired
    private SpringRedisTools springRedisTools;


    @ApiOperation(value = "客户注册", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name="loginName",value="登陆账号",required=true,paramType="query"),
            @ApiImplicitParam(name="password",value="登陆口令（使用32位小写MD5密文，如0192023a7bbd73250516f069df18b500）",required=true,paramType="query"),
            @ApiImplicitParam(name="name",value="姓名",required=true,paramType="query"),
            @ApiImplicitParam(name="verificationCode",value="验证码",required=true,paramType="query")
    })
    @OperationLog(moduleName= OperationLogEnum.MODULE_COMMON,businessName="客户注册",operationType= OperationLogEnum.Insert)
    @RequestMapping(value = "/register",method = RequestMethod.POST,produces ="application/json;charset=utf-8")
    public @ResponseBody
    WebResponseDto register(@RequestParam String loginName,
                            @RequestParam String password,
                            @RequestParam String name,
                            @RequestParam String verificationCode) {
        WebResponseDto responseDto = new WebResponseDto();

        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        if(MyStringUtils.isBlank(loginName)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("注册失败，缺少登录账号");
            return responseDto;
        }

        if(MyStringUtils.isBlank(password)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("注册失败，缺少账号密码");
            return responseDto;
        }
        if(MyStringUtils.isBlank(verificationCode)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("注册失败，缺少验证码");
            return responseDto;
        }

        String key = String.format(RedisKeyEnum.REDIS_KEY_STRING_VERIFICATION_CODE.getStringValue(),"1",loginName,verificationCode);
        if(!springRedisTools.hasKey(key)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("注册失败，无效的验证码");
            return responseDto;
        }
        springRedisTools.deleteByKey(key);


        SysCustomer customer = new SysCustomer();
        customer.setLoginName(loginName);
        customer.setName(name);
        customer.setStatus("A100201");
        customer.setCreaterId(ctx.getAccountId());
        customer.setCreaterName(ctx.getAccountName());
        customer.setCreateTime(new Date());


        String passwordRC4 = RC4.encry_RC4_string(password.toUpperCase(), ApplicationEnum.LOGIN_RC4_KEY.getStringValue());
        SysAccount account = new SysAccount();
        account.setAccountCode(loginName);
        account.setAccountName(name);
        account.setType("A101705"); // 客户账号
        account.setPassword(passwordRC4);
        account.setSource("开放接口");
        account.setCreaterId(ctx.getAccountId());
        account.setCreaterName(ctx.getAccountName());
        account.setCreateTime(new Date());
        sysCustomerServiceImpl.registerAccount(account,customer);
        return responseDto;
    }
}
