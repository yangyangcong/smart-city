/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysAppTokenMapper;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.entity.admin.apptoken.SysAppToken;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: AppToken缓存
 *
 */
@EnableComplexCaching(dependsOnPOSimpleName = "SysAppToken")
@Component
public class RedisCacheAppTokenBo extends CacheAbstract {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysAppTokenMapper sysAppTokenMapper;

    @Override
    public void init() {
        Set<String> keys = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_HASH_GROUP_APP_TOKEN_BY_APP_ID.getStringValue().replaceAll("%s_","*"));
        if(keys != null && keys.size() > 0){
            springRedisTools.deleteByKey(keys);
        }

        List<SysAppToken> appTokenList = sysAppTokenMapper.selectAll();
        if(CollectionUtil.isNotEmpty(appTokenList)){
            for (SysAppToken appToken : appTokenList) {
                cachedByAppId(appToken);
            }
        }
    }

    @Override
    public void reset() {

    }

    @Override
    public void add(Object obj){
        if(obj == null){
            return;
        }

        SysAppToken entity = null;

        if(obj instanceof Long){
            entity = sysAppTokenMapper.selectByPrimaryKey(obj);
        }else if(obj instanceof SysAppToken){
            entity = (SysAppToken)obj;
        }else if(obj instanceof Map){
            entity = JSONObject.parseObject(JSONObject.toJSONString(obj),SysAppToken.class);
        }

        if(entity == null){
            return;
        }

        cachedByAppId(entity);
    }

    @Override
    public void batchAdd(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                add(o);
            }
        }
    }

    @Override
    public void updateByPrimaryKey(Object primaryKey){
        SysAppToken token = sysAppTokenMapper.selectByPrimaryKey(primaryKey);
        if(token != null){
            cachedByAppId(token);
        }
    }

    @Override
    public void batchUpdate(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                updateByPrimaryKey(o);
            }
        }
    }

    public void deleteByPrimaryKey(Object primaryKey){
        SysAppToken token = sysAppTokenMapper.selectByPrimaryKey(primaryKey);
        if(token != null){
            String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_APP_TOKEN_BY_APP_ID.getStringValue(),token.getAppId());
            if(springRedisTools.hasKey(key1)){
                springRedisTools.deleteByKey(key1);
            }
        }
    }

    public void batchDelete(List<Object> primaryKeys){
        if(CollectionUtil.isNotEmpty(primaryKeys)){
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }

    private void cachedByAppId(SysAppToken appToken){
        String appId = appToken.getAppId();
        String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_APP_TOKEN_BY_APP_ID.getStringValue(),appId);
        if(springRedisTools.hasKey(key1)){
            springRedisTools.deleteByKey(key1);
        }
        Map mapValue1 = BeanUtil.beanToMap(appToken);
        springRedisTools.addMap(key1,mapValue1);
    }
}
