package com.sc.admin.api;

import com.sc.admin.api.fallback.ImportExportServiceFallback;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.importexport.SysImportExport;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "admin-server",fallback = ImportExportServiceFallback.class)
public interface ImportExportService {
    String API_PREFIX = "/api/feign/v1/ImportExportFeignApi";
    String INSERT = API_PREFIX + "/insert";
    String UPDATE_BY_PRIMARY_KEY_SELECTIVE = API_PREFIX + "/updateByPrimaryKeySelective";
    String SELECT_ONE = API_PREFIX + "/selectOne";

    @RequestMapping(value = INSERT,method= RequestMethod.POST, consumes = "application/json")
    WebResponseDto insert(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysImportExport entity);

    @RequestMapping(value = SELECT_ONE,method= RequestMethod.POST, consumes = "application/json")
    WebResponseDto selectOne(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysImportExport search);

    @RequestMapping(value = UPDATE_BY_PRIMARY_KEY_SELECTIVE,method= RequestMethod.POST, consumes = "application/json")
    WebResponseDto updateByPrimaryKeySelective(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysImportExport entity);
}
