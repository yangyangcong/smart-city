package com.sc.workorder.entity;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-04-01 10:10:29
 * @description:
 */
public class WoWorkOrderTypeSearch extends WoWorkOrderType {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}