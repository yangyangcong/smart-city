/**
 * Created by wust on 2020-04-18 17:33:47
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.order.common.config;

import com.sc.common.BaseSwaggerConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author: wust
 * @date: Created in 2020-04-18 17:33:47
 * @description:
 *
 */
@EnableOpenApi
@Configuration
public class SwaggerConfig extends BaseSwaggerConfig {

    @Bean
    @Override
    public Docket createRestApi() {
        return super.createRestApi();
    }

    @Override
    protected String getScanPackage() {
        return "com.sc.order.core.api";
    }

    public ApiInfo defaultTitleInfo() {
        return new ApiInfoBuilder().title("智慧社区~订单服务管理文档")
                // 详细描述
                .description("")
                .contact(new Contact("", "", ""))
                // 版本
                .version("2.0")
                .build();
    }
}
