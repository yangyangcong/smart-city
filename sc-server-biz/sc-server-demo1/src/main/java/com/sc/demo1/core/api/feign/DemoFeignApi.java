/**
 * Created by wust on 2019-11-27 13:57:02
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.demo1.core.api.feign;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.common.annotations.FeignApi;
import com.sc.common.dto.WebResponseDto;
import com.sc.demo1.entity.demo.Demo;
import com.sc.demo1.api.DemoService;
import com.sc.demo1.core.dao.DemoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @author: wust
 * @date: Created in 2019-11-27 13:57:02
 * @description:
 *
 */
@FeignApi
@RestController
public class DemoFeignApi implements DemoService {
    @Autowired
    private DemoMapper demoMapper;

    @Override
    @RequestMapping(value = SELECT,method= RequestMethod.POST)
    public WebResponseDto select(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody Demo search) {
        WebResponseDto responseDto = new WebResponseDto();
        List<Demo> list = demoMapper.select(search);
        if(CollectionUtil.isNotEmpty(list)){
            responseDto.setObj(JSONObject.toJSONString(list));
        }
        return  responseDto;
    }
}
