/**
 * Created by wust on 2019-11-07 09:05:47
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.demo2.core.api.open;

import com.sc.common.annotations.OpenApi;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: wust
 * @date: Created in 2019-11-07 09:05:47
 * @description:
 *
 */
@OpenApi
@RequestMapping("/api/open/v1/DemoOpenApi")
@RestController
public class DemoOpenApi {


}
