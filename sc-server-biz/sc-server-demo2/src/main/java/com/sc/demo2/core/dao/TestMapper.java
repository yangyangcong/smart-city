package com.sc.demo2.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.demo2.entity.test.Test2;

/**
 * @author: wust
 * @date: 2020-07-04 13:29:41
 * @description:
 */
public interface TestMapper extends IBaseMapper<Test2>{
}