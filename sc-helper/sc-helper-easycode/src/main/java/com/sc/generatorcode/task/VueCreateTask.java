package com.sc.generatorcode.task;

import com.sc.generatorcode.entity.ColumnInfo;
import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import com.sc.generatorcode.utils.StringUtil;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author ：wust
 * @date：2019-12-26
 */
public class VueCreateTask extends AbstractTask {
    public VueCreateTask(String className, List<ColumnInfo> infos) {
        super(className,infos);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String postfixName =  super.getName()[0];
        String name = super.getName()[1];


        StringBuffer formItemStr = new StringBuffer();
        StringBuffer formModelPropertyStr = new StringBuffer();
        if(columnInfos != null && columnInfos.size() > 0){
            int i = 0;
            for (ColumnInfo columnInfo : columnInfos) {
                if("id".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createrId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createrName".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createTime".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyName".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyTime".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("projectId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("companyId".equals(columnInfo.getPropertyName())){
                    continue;
                }

                formItemStr.append("<el-form-item label=\"" + columnInfo.getColumnLabel() + "\" prop=\"" + columnInfo.getPropertyName() + "\">\n");
                formItemStr.append("<el-input v-model=\"formModel." + columnInfo.getPropertyName() + "\"></el-input>\n");
                formItemStr.append("</el-form-item>\n");


                if( i ++ == columnInfos.size()){
                    formModelPropertyStr.append(columnInfo.getPropertyName() + ": null");
                }else{
                    formModelPropertyStr.append(columnInfo.getPropertyName() + ": null,\n");
                }
            }
        }


        Map<String, String> daoData = new HashMap<>();
        daoData.put("Author", ConfigUtil.getConfiguration().getAuthor());
        daoData.put("DateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        daoData.put("moduleName", postfixName);
        daoData.put("name", name);
        daoData.put("formItem",formItemStr.toString());
        daoData.put("formModelProperty",formModelPropertyStr.toString());
        daoData.put("pageName", postfixName + "List");
        daoData.put("axiosReqPrefix", ConfigUtil.getConfiguration().getAxiosReqPrefix());

        String filePath = StringUtil.package2Path(ConfigUtil.getConfiguration().getPath().getVueDir());
        String fileName = name + "-create.vue";

        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_VUE_CREATE, daoData, filePath + File.separator + name,fileName);
    }
}