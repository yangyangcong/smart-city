/**
 * Created by wust on 2019-10-26 09:31:00
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.mq.producer;

import com.alibaba.fastjson.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author: wust
 * @date: Created in 2019-10-26 09:31:00
 * @description: FanoutExchange专用生产者
 *
 */
@Component
public class Producer4fanoutExchange {
    static Logger logger = LogManager.getLogger(Producer4fanoutExchange.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private Environment env;

    public void send(String exchangeName,JSONObject jsonObject){
        if(logger.isInfoEnabled()){
            logger.info("发送给指定exchange[{}]的生产者开始发送工作。。。",exchangeName);
        }

        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        rabbitTemplate.setExchange(exchangeName);
        rabbitTemplate.convertAndSend(jsonObject);

        if(logger.isInfoEnabled()){
            logger.info("发送给指定exchange[{}]的生产者完成发送工作。。。",exchangeName);
        }
    }
}
