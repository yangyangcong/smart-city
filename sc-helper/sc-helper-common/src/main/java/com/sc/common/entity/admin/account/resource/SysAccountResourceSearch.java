/**
 * Created by wust on 2020-03-26 09:48:07
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.common.entity.admin.account.resource;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: Created in 2020-03-26 09:48:07
 * @description:
 *
 */
public class SysAccountResourceSearch extends SysAccountResource {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}
