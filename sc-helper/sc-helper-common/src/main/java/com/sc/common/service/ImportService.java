package com.sc.common.service;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.dto.WebResponseDto;

public interface ImportService {
    WebResponseDto importByExcel(JSONObject jsonObject);
}
