package com.sc.minio;


import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import com.sc.common.exception.BusinessException;
import com.sc.common.service.MinioStorageApiService;
import com.sc.common.service.MinioStorageService;
import io.minio.*;
import io.minio.http.Method;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Created by wust on 2019/5/17.
 */

@Service
public class MinioStorageApiServiceImpl implements MinioStorageApiService {
    static Logger logger = LogManager.getLogger(MinioStorageApiServiceImpl.class);

    @Autowired
    private Environment environment;

    @Autowired
    private MinioClient minioClient;


    /**
     * try {
     * ByteArrayInputStream bais = new ByteArrayInputStream(builder.toString().getBytes("UTF-8"));
     * KeyGenerator keyGen = KeyGenerator.getInstance("AES");
     * keyGen.init(256);
     * ServerSideEncryption sse = ServerSideEncryption.withCustomerKey(keyGen.generateKey());
     * Map<String, String> headerMap = new HashMap<>();
     * headerMap.put("my-custom-data", "foo");
     * minioClient.putObject("mybucket",  "island.jpg", bais,headerMap,sse, "application/octet-stream" );
     * System.out.println("island.jpg is uploaded successfully");
     * } catch(MinioException e) {
     * System.out.println("Error occurred: " + e);
     * }
     *
     * @param objectName  Name of the bucket
     * @param stream      stream to upload
     * @param size        Size of the file
     * @param headerMap   Custom/additional meta data of the object
     * @param sse         Form of server-side encryption ServerSideEncryption
     * @param contentType File content type of the object, user supplied
     * @return
     */
    public SysDistributedFile upload(String objectName, InputStream stream, Long size, Map<String, String> headerMap, Object sse, String contentType, String fileName, String fileType, String source, MinioStorageService minioStorageService) {
        String bucketName = environment.getProperty("minio.bucket-name");
        try {
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if(!found){
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
            }
        } catch (Exception e){
            logger.error(e);
            throw new BusinessException("上传失败，文件服务器异常。" + e.getMessage());
        }
        return minioStorageService.upload(bucketName,objectName,stream,size,contentType,fileName,fileType,source);
    }


    public InputStream download(Long fileId, MinioStorageService minioStorageService) {
        try {
            SysDistributedFile distributedFile = minioStorageService.selectDistributedFileByPrimaryKey(fileId);
            if(distributedFile != null){
                String bucketName = distributedFile.getBucketName();
                String objectName = distributedFile.getObjectName();

                InputStream stream = minioClient.getObject(
                        GetObjectArgs.builder() .bucket(bucketName).object(objectName).build()
                );
                return stream;
            }else{
                throw new BusinessException("对应的文件不存在于文件系统。");
            }
        } catch (Exception e) {
            logger.error("下载文件出错：" + e);
            throw new BusinessException("下载文件异常：" + e.getMessage());
        }
    }


    public void delete(String bucketName, String objectName, MinioStorageService minioStorageService) {
        minioStorageService.delete(bucketName,objectName);
    }


    @Override
    public String getObjectUrl(String bucketName,String objectName){
        String url = "";
        try {
            url = minioClient.getObjectUrl(bucketName,objectName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }

    @Override
    public String getPresignedObjectUrl(String bucketName, String objectName) {
        String url = "";
        try {
            url = minioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs.builder()
                            .method(Method.GET)
                            .bucket(bucketName)
                            .object(objectName)
                            .expiry(2, TimeUnit.MINUTES)
                            .build());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }
}
